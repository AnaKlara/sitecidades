
//-----------menu
$(function(){
 	$(".button-collapse").sideNav();
 });


//-----------Select

  $(document).ready(function() {
    $('select').material_select();
  });
 


 //---Layout Moisaico

 // jQuery
$('.grid').masonry({
  columnWidth: '.grid-item',
  itemSelector: '.grid-item'
});


//-----Chips
var chip = {
    tag: 'chip content',
    image: '', //optional
    id: 1, //optional
};


$('.chips').material_chip();
  
$('.chips-initial').material_chip({
    data: [{
      tag: 'Aula Magna',
    }, {
      tag: 'Volta ás aulas',
    }, {
      tag: 'DCE',
    }],
  });


        

//---Data Picker -> Filtro
 $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Hoje',
    clear: 'Limpar',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });




//---------Aparecer botão back to top
 function apareceDivs(elem){
	elem.style.display = 'block';
}
//----------Scroll Fire

var options = [
    //-------Aparece botão de voltar ao topo
    {selector: '#menu-pesquisa', offset: 800, callback: function() {
      apareceDivs(topo);
    }},
  ];
  Materialize.scrollFire(options);

  //----------Voltar ao topo
  function topTop(){
    var totop = $(window).scrollTop()-8;
    if(totop <= 0){
       clearInterval(idInterval);
    }else{
        totop--;
        $(window).scrollTop(totop);
    }
}
 
function levTop(){
    idInterval = setInterval('topTop();', 1);
}