
//-----------menu
$(function(){
 	$(".button-collapse").sideNav();
 });

//---------IMAGENS PARALAX
$(document).ready(function(){
    $('.parallax').parallax();
   });

// Pause slider
$('.slider').slider('pause');
// Start slider
$('.slider').slider('start');
// Next slide
$('.slider').slider('next');
// Previous slide
$('.slider').slider('prev');

//------------Imagem Grande
$(document).ready(function(){
    $('.materialboxed').materialbox();
  });

//------------FAQ
$(document).ready(function(){
    $('.collapsible').collapsible();
  });

//--Para aparecer ícone no toast
const Icon = '<i class="material-icons print">touch_app</i>';
const Message = 'Para +Interação';
const $toast = Icon +Message ;

//----------Scroll Fire

var options = [
	//-------1º imagem paralax
    {selector: '#palma', offset: 300, callback: function() {
      moveED(Ptit, 25);
      moveDE(Ptexto, 25);
    }},
    //-------2º imagem paralax
    {selector: '#exemplo', offset: 400, callback: function() {
      moveDE2(Exetit, 25);
      moveED2(Exetexto, 25);
    }},
    //-------3º imagem paralax: conectividade
    {selector: '#conectividade', offset: 400, callback: function() {
      moveCB(Conectit, 25);
      moveBC(ConectTexto, 25);
    }},
    //-------4º vídeo conectividade
    {selector: '#transito', offset: 400, callback: function() {
      moveED(trantit, 25);
      moveDE(trantexto, 25);
    }},
    //-------5º imagem paralax: mãos
    {selector: '#maos', offset: 400, callback: function() {
      moveDE2(matit, 25);
      moveED2(matexto, 25);
    }},
    //-------6º imagem paralax: baixe o app
    {selector: '#apelo', offset: 400, callback: function() {
      moveCB(apelotit, 25);
      moveBC(apelotexto, 25);
    }},
    //-------Aparece botões de menu e convite
    {selector: '#palma', offset: 200, callback: function() {
      apareceDivs(convite, menuReserva);
    }},
    //-------Aparece botões de menu e convite no mobile
    {selector: '#palma', offset: 100, callback: function() {
      apareceDivs(menuReservaM, menuReservaM2);
    }},
    //-----Aparece toast Funcionalidades
    {selector: '#funcionalidades', offset: 400, callback: function() {
       Materialize.toast($toast, 4000 );
    }},
  ];
  Materialize.scrollFire(options);

//Vir da esquerda pra direita OO------->
function moveED(elem, duracao) {
  // valor da posição a esquerda do elemento
  var left 	= -80;
  // função a ser chamada até chegar a posição informada
  function deslocamento() {
    // incrementando contador
    left++;
    // aplicando estilo no elemento
    elem.style.left = left + '%';

    // verificando se chegou ao ponto desejado
    if (left == 0 )
      // interrompe o processo de deslocamento 
      clearInterval(id);
    }
   // desloca o elemento até 10 segundos
   // aumentando o valor, vai demorar mais para chegar
   var id = setInterval(deslocamento, duracao);
}

//Vir da direita pra esquerda <--------OO
function moveDE(elem, duracao) {
  // valor da posição a esquerda do elemento
  var right = -80;
  // função a ser chamada até chegar a posição informada
  function deslocamento() {
    // incrementando contador
    right++;
    // aplicando estilo no elemento
    elem.style.right = right + '%';

    // verificando se chegou ao ponto desejado
    if (right == 0 )
      // interrompe o processo de deslocamento 
      clearInterval(id);
    }
   // desloca o elemento até 10 segundos
   // aumentando o valor, vai demorar mais para chegar
   var id = setInterval(deslocamento, duracao);
}

//Vir da esquerda pra direita OO------->
function moveED2(elem, duracao) {
  // valor da posição a esquerda do elemento
  var left 	= -100;
  // função a ser chamada até chegar a posição informada
  function deslocamento() {
    // incrementando contador
    left++;
    // aplicando estilo no elemento
    elem.style.left = left + '%';

    // verificando se chegou ao ponto desejado
    if (left == -15 )
      // interrompe o processo de deslocamento 
      clearInterval(id);
    }
   // desloca o elemento até 10 segundos
   // aumentando o valor, vai demorar mais para chegar
   var id = setInterval(deslocamento, duracao);
}

//Vir da direita pra esquerda <--------OO
function moveDE2(elem, duracao) {
  // valor da posição a esquerda do elemento
  var right = -80;
  // função a ser chamada até chegar a posição informada
  function deslocamento() {
    // incrementando contador
    right++;
    // aplicando estilo no elemento
    elem.style.right = right + '%';

    // verificando se chegou ao ponto desejado
    if (right == 35 )
      // interrompe o processo de deslocamento 
      clearInterval(id);
    }
   // desloca o elemento até 10 segundos
   // aumentando o valor, vai demorar mais para chegar
   var id = setInterval(deslocamento, duracao);
}


//Vir de Cima Para Baixo
function moveCB(elem, duracao) {
  // valor da posição a esquerda do elemento
  var top 	= -55;
  // função a ser chamada até chegar a posição informada
  function deslocamento() {
    // incrementando contador
    top++;
    // aplicando estilo no elemento
    elem.style.top = top + '%';

    // verificando se chegou ao ponto desejado
    if (top == 25 )
      // interrompe o processo de deslocamento 
      clearInterval(id);
    }
   // desloca o elemento até 10 segundos
   // aumentando o valor, vai demorar mais para chegar
   var id = setInterval(deslocamento, duracao);
}

//Vir de Baixo Para Cima
function moveBC(elem, duracao) {
  // valor da posição a esquerda do elemento
  var top = 120;
  // função a ser chamada até chegar a posição informada
  function deslocamento() {
    // incrementando contador
    top--;
    // aplicando estilo no elemento
    elem.style.top = top + '%';

    // verificando se chegou ao ponto desejado
    if (top == 50 )
      // interrompe o processo de deslocamento 
      clearInterval(id);
    }
   // desloca o elemento até 10 segundos
   // aumentando o valor, vai demorar mais para chegar
   var id = setInterval(deslocamento, duracao);
}

function apareceDivs(elem, secondElem){
	elem.style.display = 'block';
	secondElem.style.display = 'block';
}

//mapa
function initMap() {
        var uluru = {lat: -22.772842, lng: -43.685852};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }